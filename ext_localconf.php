<?php

defined('TYPO3') or die();

(function() {
	
	$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase('fidelo'));

	$typoVersionClass = new TYPO3\CMS\Core\Information\Typo3Version;
	$typoVersion = $typoVersionClass->getVersion();

	// Without cache
	if (version_compare($typoVersion, '11.0.0', '<')) {
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
			'Fidelo',
			'Fidelosnippet',
			['Default' => 'render'],
			// non-cacheable actions
			['Default' => 'render']
		);
	} else {
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
			'Fidelo',
			'Fidelosnippet',
			[\Fidelo\Fidelo\Controller\DefaultController::class => 'render'],
			// non-cacheable actions
			[\Fidelo\Fidelo\Controller\DefaultController::class => 'render']
		); 
	}

	// With cache
	if (version_compare($typoVersion, '11.0.0', '<')) {
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
			'Fidelo',
			'FidelosnippetCached',
			['Default' => 'render']
		);
	} else {
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
			'Fidelo',
			'FidelosnippetCached',
			[\Fidelo\Fidelo\Controller\DefaultController::class => 'render']
		);
	}
	
	$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
	$iconRegistry->registerIcon('fidelo_icon', \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class, ['source' => 'EXT:'.$extensionName.'/Resources/Public/Icons/Extension.svg']);

})();
