<?php

defined('TYPO3') or die('ext');

(function() {
	
	$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase('fidelo'));
	
	// Without cache
	$pluginName = strtolower('Fidelosnippet');
	#$pluginSignature = $extensionName.'_'.$pluginName;

	$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
		'Fidelo',
		'Fidelosnippet',
		'Fidelo Snippet',
		'fidelo_icon'
	);

	$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
	$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';

	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'.$extensionName.'/flexform.xml');

	// With cache
	$pluginNameCached = strtolower('FidelosnippetCached');

	$pluginSignatureCached = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
		'Fidelo',
		'FidelosnippetCached',
		'Fidelo Snippet Cached',
		'fidelo_icon'
	);

	$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignatureCached] = 'layout,select_key,pages,recursive';
	$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignatureCached] = 'pi_flexform';

	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignatureCached, 'FILE:EXT:'.$extensionName.'/flexform.xml');

	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extensionName, 'Configuration/TypoScript', 'Fidelo');

	#if (TYPO3_MODE == 'BE') {
		$GLOBALS['TBE_MODULES_EXT']['xMOD_db_new_content_el']['addElClasses']['Fidelo\Fidelo\Wizicon'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extensionName).'Classes/Wizicon.php';
	#}
})();
