<?php

########################################################################
# Extension Manager/Repository config file for ext "fidelo".
#
# Auto generated 26-08-2011 18:11
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Fidelo Typo3 API',
	'description' => '',
	'category' => 'plugin',
	'author' => 'Fidelo Software GmbH',
	'author_email' => 'marketing@fidelo.com',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => 'Fidelo Software GmbH',
	'version' => '4.4.1',
	'constraints' => array(
		'depends' => array(
			'typo3' => '12.0.0-12.9.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'suggests' => array(
	),
	'autoload' => [
		'psr-4' => [
			'Fidelo\\Fidelo\\' => 'Classes'
		]
	],
);
