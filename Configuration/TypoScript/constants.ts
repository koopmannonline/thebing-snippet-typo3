
plugin.tx_fidelo_fidelosnippet {
  view {
    # cat=plugin.tx_fidelo_fidelosnippet/file; type=string; label=Path to template root (FE)
    templateRootPath = EXT:fidelo/Resources/Private/Templates/
    # cat=plugin.tx_fidelo_fidelosnippet/file; type=string; label=Path to template partials (FE)
    partialRootPath = EXT:fidelo/Resources/Private/Partials/
    # cat=plugin.tx_fidelo_fidelosnippet/file; type=string; label=Path to template layouts (FE)
    layoutRootPath = EXT:fidelo/Resources/Private/Layouts/
  }
  persistence {
    # cat=plugin.tx_fidelo_fidelosnippet//a; type=string; label=Default storage PID
    storagePid =
  }
}
