
plugin.tx_fidelo_fidelosnippet {
  view {
    templateRootPaths.0 = EXT:fidelo/Resources/Private/Templates/
    templateRootPaths.1 = {$plugin.tx_fidelo_fidelosnippet.view.templateRootPath}
    partialRootPaths.0 = EXT:fidelo/Resources/Private/Partials/
    partialRootPaths.1 = {$plugin.tx_fidelo_fidelosnippet.view.partialRootPath}
    layoutRootPaths.0 = EXT:fidelo/Resources/Private/Layouts/
    layoutRootPaths.1 = {$plugin.tx_fidelo_fidelosnippet.view.layoutRootPath}
  }
  persistence {
    storagePid = {$plugin.tx_fidelo_fidelosnippet.persistence.storagePid}
    #recursive = 1
  }
  features {
    #skipDefaultArguments = 1
  }
  mvc {
    callDefaultActionIfActionCantBeResolved = 1
  }
}
