<?php

namespace Fidelo\Fidelo\Listener;

class FlushCache {
	
	public function handleEvent() {
	
		$aExtensionConfiguration = $this->getExtensionConfig();

		if(empty($aExtensionConfiguration) || !isset($aExtensionConfiguration['url'])) {
			// No config
			return;
		}

		$sUrl = $aExtensionConfiguration['url'];

		$sProxyPath = \TYPO3\CMS\Core\Core\Environment::getPublicPath().DIRECTORY_SEPARATOR.'thebing_proxy.php';

		if(file_exists($sProxyPath)) {
			$sContent = file_get_contents($sProxyPath);
			// the current thebing_proxy.php ist up to date
			if(strpos($sContent, $sUrl) !== false) {
				return;
			}
		}

		// write proxy file based on template file

		$sProxyTemplate = file_get_contents(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('fidelo').'thebing_proxy.php');

		$sProxyContent = str_replace('{FIDELO_URL}', $sUrl, $sProxyTemplate);

		file_put_contents($sProxyPath, $sProxyContent);

		$sTestUrl = $sUrl.'/system/extensions/tc_api.php?task=check_installation';

		// Fallback, file_get_contents darf vielleicht keine URLs öffnen
		if(function_exists('curl_init')) {
			$rCurl = curl_init($sTestUrl);
			curl_setopt($rCurl, CURLOPT_RETURNTRANSFER, true);
			$sTest = curl_exec($rCurl);
			curl_close($rCurl);
		} else {
			$sTest = file_get_contents($sTestUrl);
		}

		if($sTest !== 'ok') {
			// Fehler anzeigen
		}

	}

	/**
	 * Get Extension configuration based on typo3 version
	 *
	 * @return array|mixed
	 */
	private function getExtensionConfig() {

		if(isset($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['fidelo'])) {
			// Typo3 9
			return unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['fidelo']);
		} else if(isset($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['fidelo'])) {
			// Typo3 10
			return $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['fidelo'];
		}

		return [];
	}
	
}
