<?php

namespace Fidelo\Fidelo\Controller;

use Snoopy;

class DefaultController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	public function renderAction() {

		include_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('fidelo')."/Includes/class.snippet.php");
		
		$aExtensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['fidelo']??null);
		
		$sUrl = $this->settings['url'];
		
		if(
			empty($sUrl) &&
			!empty($aExtensionConfiguration['url'])
		) {
			$sUrl = $aExtensionConfiguration['url'];
		}
		
		$sType = $this->settings['type'];
		$sCode = $this->settings['code'];
		$sTemplate = $this->settings['template'];
		$sKey = $this->settings['key']??'';
		$sFormId = $this->settings['form']??'';
		$sLanguage = $this->settings['language']??'';

		$sContent = '';
		switch($sType) {
			case 'ts_feedback':
				$sContent = self::getTsFeedback($sUrl, $sKey, $sLanguage);
				break;
			case 'ts_placementtest':
				$sContent = self::getPlacementTest($sUrl, $sKey, $sLanguage);
				break;
			case 'ts_registration':
				$sKey = $sFormId.'='.$sKey;
				$sContent = self::getRegistrationForm($sUrl, $sKey, $sLanguage);
				break;
			case 'default':
				$sContent = self::getDefault($sUrl, $sCode, $sTemplate);
				break;
		}

		$this->view->assign('content', $sContent);

		return $this->htmlResponse();
	}

	/**
	 * @param string $sServer
	 * @param string $sKey
	 * @param string $sLanguage
	 * @return string
	 */
	private static function getTsFeedback($sServer, $sKey, $sLanguage) {

		$aSubmitVars['r'] = isset($_GET['r']) ? $_GET['r'] : '';
		$aSubmitVars['KEY'] = $sKey;
		$aSubmitVars['save'] = $_POST['save'];
		$aSubmitVars['sLanguage'] = $sLanguage;
		$aSubmitVars['pid'] = $_SESSION['__pid'];
		$aSubmitVars['pp'] = $_SESSION['__ppa'];

		if($_REQUEST['task'] == 'detail' and $_REQUEST['action'] == 'save') {
			foreach($_REQUEST as $key => $value) {
				$aSubmitVars[$key] = $value;
			}
		}

		$oSnoopy = new Snoopy();
		$oSnoopy->submit($sServer . '/system/extensions/kolumbus_feedback.php', $aSubmitVars);
		$sResults = $oSnoopy->results;

		return $sResults;
	}

	/**
	 * @param string $sServer
	 * @param string $sKey
	 * @param string $sLanguage define the language of the site, the default Language of the school is used if it is not defined
	 * @param string $iCurrencyId define the Currency of the Site by ID , otherwise the first currency or $_VARS['sCurrency'] is used
	 * @param string $sCurrencyIso define the Currency of the Site by ISO name, otherwise the first currency or $_VARS['idCurrency'] is used
	 * @return string
	 */
	private static function getPlacementTest($sServer, $sKey, $sLanguage = '', $iCurrencyId = '', $sCurrencyIso = '') {

		$aSubmitVars['r'] = $_REQUEST['r'];
		$aSubmitVars['KEY'] = $sKey;
		$aSubmitVars['save'] = $_POST['save'];
		$aSubmitVars['isPeriod'] = $_POST['idPeriod'];

		if($sLanguage !== '') {
			$aSubmitVars['page_language'] = $sLanguage;
		}
		if($iCurrencyId !== '') {
			$aSubmitVars['idCurrency'] = $iCurrencyId;
		}
		if($sCurrencyIso !== '') {
			$aSubmitVars['sCurrency'] = $sCurrencyIso;
		}

		$oSnoopy = new Snoopy();
		$oSnoopy->submit($sServer . '/system/extensions/kolumbus_placementtest.php', $aSubmitVars);
		$sResults = $oSnoopy->results;

		return $sResults;
	}

	/**
	 * @param string $sServer
	 * @param string $sKey
	 * @param string $sLanguage
	 * @return string
	 */
	private static function getRegistrationForm($sServer, $sKey = '', $sLanguage = '') {

		$aSubmitVars = array();

		if(!empty($_REQUEST)) {
			foreach((array)$_REQUEST as $mKey=>$mValue) {
				$aSubmitVars[$mKey] = $mValue;
			}
		}
		if($sKey !== '') {
			$aSubmitVars['form_key'] = $sKey;
		}
		if($sLanguage !== '') {
			$aSubmitVars['page_language'] = $sLanguage;
		}

		$oSnoopy = new Snoopy();
		$aFiles = array();

		if(!empty($_FILES)) {

			$sTempDir = sys_get_temp_dir();
			if(!is_writeable($sTempDir)) {
				die('Fatal error while uploading file');
			}

			foreach((array)$_FILES as $sKey => $mItems) {
				if(!is_array($mItems['name'])) {
					$sTarget = $sTempDir . '/' . $mItems['name'];
					if(move_uploaded_file($mItems['tmp_name'], $sTarget)) {
						$aFiles[$sKey] = $sTarget;
					}
				} else {
					self::prepareFiles($mItems['name'], $mItems['tmp_name'], $aFiles[$sKey], $sTempDir);
				}
			}

		}

		$aSubmitVars['X-Originating-IP'] = $_SERVER['REMOTE_ADDR'];
		$aSubmitVars['X-Originating-Agent'] = $_SERVER['HTTP_USER_AGENT'];
		$aSubmitVars['X-Originating-Host'] = $_SERVER['HTTP_HOST'];
		$aSubmitVars['X-Originating-URI'] = $_SERVER['REQUEST_URI'];
		$aSubmitVars['X-Originating-HTTPS'] = $_SERVER['HTTPS'];
		
		$oSnoopy->cookies = $_COOKIE;
		unset($aSubmitVars['PHPSESSID']);

		$oSnoopy->set_submit_multipart();
		$oSnoopy->submit($sServer . '/system/extensions/thebing_registration_form.php?'.$_SERVER['QUERY_STRING'], $aSubmitVars, $aFiles);
		$sResults = $oSnoopy->results;

		if(
			isset($_REQUEST['task']) &&
			(
				$_REQUEST['task'] == 'get_js' ||
				$_REQUEST['task'] == 'get_image' ||
				$_REQUEST['task'] == 'get_file' ||
				$_REQUEST['task'] == 'get_ajax'
			)
		) {
			foreach((array)$oSnoopy->headers as $sHeader) {
				if(strpos($sHeader, 'Content-Type') !== false) {
					header($sHeader);
				}
			}
			ob_clean();
			echo $oSnoopy->results;
			die();
		}

		// Make internal server error of registration form recognizably
		if($oSnoopy->status == 500) {
			$sResults = 'Fatal error of registration form!';
		}

		// If content is already sent, no cookies can be set afterwards.
		// That's deadly for the function of the registration form, so that's a fatal error.
		// Usually this is an user error!
		if(headers_sent()) {
			$sResults = 'Wrong order of content output. Check whether you\'ve no output before including of registration form!';
		}

		foreach((array)$oSnoopy->cookies as $sKey=>$mValue) {
			if(function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) {
				$mValue = stripslashes($mValue);
			}
			if(is_scalar($mValue)) {
				setcookie($sKey, $mValue);
			}
		}

		foreach((array)$oSnoopy->headers as $sHeader) {
			if(function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) {
				$sHeader = stripslashes($sHeader);
			}
			if(strpos($sHeader, 'Content-Type') !== false) {
				header($sHeader, true);					
			}
		}
		
		if(!empty($aFiles)) {
			self::unlinkFiles($aFiles);
		}

		return $sResults;
	}

	/**
	 * @param string $sServer
	 * @param string $sCombinationKey
	 * @param string $sTemplateKey
	 * @return string
	 */
	private static function getDefault($sServer, $sCombinationKey, $sTemplateKey) {

		try {
			static::appendFideloPageConfig($_REQUEST);
		} catch (\Throwable $e) {}

		$oSnippet = new \Thebing_Snippet($sServer, $sCombinationKey, $sTemplateKey);
		$oSnippet->execute();
		$sContent = $oSnippet->getContent();

		return $sContent;
	}

	/**
	 * @param array $aFiles
	 */
	private static function unlinkFiles(&$aFiles) {

		foreach((array)$aFiles as $mKey => $mFile) {
			if(is_array($mFile)) {
				self::unlinkFiles($aFiles[$mKey]);
			} else if(is_file($mFile)) {
				unlink($mFile);
			}
		}

	}

	/**
	 * @param $mItems
	 * @param $mTmpItems
	 * @param $aFiles
	 * @param $sTempDir
	 */
	private static function prepareFiles($mItems, $mTmpItems, &$aFiles, $sTempDir) {

		foreach((array)$mItems as $sKey => $aItems) {
			if(!is_array($aItems)) {
				$sTarget = $sTempDir . '/' . $mItems[$sKey];
				if(move_uploaded_file($mTmpItems[$sKey], $sTarget)) {
					$aFiles[$sKey] = $sTarget;
				}
			} else {
				self::prepareFiles($mItems[$sKey], $mTmpItems[$sKey], $aFiles[$sKey], $sTempDir);
			}
		}

	}

	private static function appendFideloPageConfig(array &$array) {

		$pageTsConfig = \TYPO3\CMS\Backend\Utility\BackendUtility::getPagesTSconfig($GLOBALS['TSFE']->id);
		if (!empty($pageTsConfig['fidelo.'])) {
			$fidelo = static::removeDots($pageTsConfig['fidelo.']);
			foreach ($fidelo as $key => $value) {
				$array[$key] = $value;
			}
		}
	}

	private static function removeDots(array $array) {
		$clean = [];

		foreach ($array as $key => $value) {
			$key = str_replace('.', '', $key);
			if (is_array($value)) {
				$clean[$key] = static::removeDots($value);
			} else {
				$clean[$key] = $value;
			}
		}

		return $clean;
	}


}
