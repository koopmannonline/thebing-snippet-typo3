<?php

namespace Fidelo\Fidelo;

/***************************************************************
*  Copyright notice
*
*  (c) 2011 plan-i GmbH <info@plan-i.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */




/**
 * Class that adds the wizard icon.
 *
 * @author	plan-i GmbH <info@plan-i.de>
 * @package	TYPO3
 * @subpackage	tx_thebing
 */
class Wizicon {

	/**
	 * Processing the wizard items array
	 *
	 * @param	array		$wizardItems: The wizard items
	 * @return	Modified array with wizard items
	 */
	function proc($wizardItems)	{
		
		$wizardItems['plugins_tx_fidelo_fidelosnippet'] = array(
			'iconIdentifier'=> 'fidelo_icon',
			'title'=> 'Fidelo Snippet',
			'description'=> '',
			'params'=>'&defVals[tt_content][CType]=list&defVals[tt_content][list_type]=fidelo_fidelosnippet'
		);

		return $wizardItems;
	}

}
